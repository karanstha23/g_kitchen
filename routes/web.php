<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [App\Http\Controllers\HomepageController::class, 'index']);
Route::get('greasy_spoon/menu', [App\Http\Controllers\HomepageController::class, 'menu']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

Route::resource('admin/home/booking', App\Http\Controllers\BookingController::class);
Route::resource('admin/home/menu', App\Http\Controllers\MenuController::class);
Route::get('admin/home/menu/activate/{id}', [App\Http\Controllers\MenuController::class,'activate']);
Route::get('admin/home/menu/deactivate/{id}', [App\Http\Controllers\MenuController::class,'deactivate']);
Route::get('admin/home/{id}', [App\Http\Controllers\HomeController::class, 'confirmed']);
Route::get('admin/greasy_kitchen/message', [App\Http\Controllers\ContactUsController::class, 'message']);
Route::resource('greasy_spoon/contact', App\Http\Controllers\ContactUsController::class);
Route::get('/greasy_spoon/home-cook', [App\Http\Controllers\HomepageController::class, 'homeCook']);
