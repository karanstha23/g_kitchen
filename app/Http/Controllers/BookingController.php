<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use Illuminate\Http\Request;
use Auth;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return('here');
        $validation = $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'contact_number' => 'required|min:10',
            'guest' => 'required',
            'time' => 'required',
            'date' => 'required|date_format:Y-m-d|after:today',
            'user_id' => 'integer',

        ]);

            BookingModel::create($request->all());
            return redirect()->back()->with('success', 'Booking created sucessfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookingModel  $bookingModel
     * @return \Illuminate\Http\Response
     */
    public function show(BookingModel $bookingModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookingModel  $bookingModel
     * @return \Illuminate\Http\Response
     */
    public function edit(BookingModel $bookingModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BookingModel  $bookingModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookingModel $bookingModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookingModel  $bookingModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookingModel $bookingModel)
    {
        //
    }
}
