<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuModel;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = MenuModel::All();
        return view('menu.view_menu')->with('menus', $menus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create_menu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'dish_type' => 'required',
            'dish_name' => 'required',
            'dish_description' => 'required',
            'dish_price' => 'required',


        ]);

            MenuModel::create($request->all());
            return redirect()->route('menu.index')->with('success', 'Menu Created Sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = MenuModel::find($id);
        return view('menu.edit_menu')->with('menus', $menus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'dish_type' => 'required',
            'dish_name' => 'required',
            'dish_description' => 'required',
            'dish_price' => 'required',


        ]);
        $menu= MenuModel::find($id);
        $menu->dish_type = $request->input('dish_type');
        $menu->dish_name = $request->input('dish_name');
        $menu->dish_description = $request->input('dish_description');
        $menu->dish_price = $request->input('dish_price');

            $menu->save();
            return redirect()->route('menu.index')->with('success', 'Menu created sucessfully');
    }
    public function activate($id, Request $request)
    {
        $menu = MenuModel::find($id);
        $menu->is_choice = 1;
        $menu->save();
        return redirect()->back()->with('success', 'Menu Activated Sucessfully');
    }
    public function deactivate($id, Request $request)
    {
        $menu = MenuModel::find($id);
        $menu->is_choice = 0;
        $menu->save();
        return redirect()->back()->with('success', 'Menu Deactivated Sucessfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
