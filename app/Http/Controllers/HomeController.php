<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\BookingModel;
use App\Mail\Gmail;
use Auth;
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()){
            $id= Auth::user()->id;
        }
        $bookings = DB::table('booking')->where('user_id', $id)->get();
        // return $bookings;
        return view('home')->with('bookings', $bookings);

    }
    public function adminHome()

    {
        $bookings = BookingModel::All();
        return view('adminHome')->with('bookings', $bookings);
        // return view('');

    }
    public function confirmed($id, Request $request)
    {
        $bookings = BookingModel::find($id);

        $bookings->is_accepted = 1;
        $data = $bookings->date;
        $details = array(
            'title' => 'Thank you for choosing us',
            'body' => 'Your booking has been confirmed',
            'date' => $data
        );


        // return($details);
        Mail::to($bookings->email)->send(new Gmail($details));
        $bookings->save();

        return redirect()->back();
    }
}
