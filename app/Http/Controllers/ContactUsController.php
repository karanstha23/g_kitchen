<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Models\ContactModel;
use Illuminate\Http\Request;
use App\Mail\Message;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }
    public function message()
    {
        $messages = ContactModel::All();
        return view('admin_message')->with('messages', $messages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = $request->validate([
            'name' => 'required',
            'email' => 'email|required',
            'contact_number' => 'required|min:10',
            'message' => 'required',
        ]);
        // return('here');
        $details = array(
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'contact_number' => $request->input('contact_number'),
            'message' => $request->input('message'),
        );


        // return($details);
        Mail::to('karanstha23@gmail.com')->send(new Message($details));

            ContactModel::create($request->all());
            return redirect()->back()->with('success', 'Message Sent Sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContactModel  $contactModel
     * @return \Illuminate\Http\Response
     */
    public function show(ContactModel $contactModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContactModel  $contactModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactModel $contactModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContactModel  $contactModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactModel $contactModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContactModel  $contactModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactModel $contactModel)
    {
        //
    }
}
