<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MenuModel;

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appetizers = MenuModel::select('*')->where([['dish_type','=','appetizer'],['is_choice','=', 1]])->get();
        $mains = MenuModel::select('*')->where([['dish_type','=','main dish'],['is_choice','=', 1]])->get();
        $desserts = MenuModel::select('*')->where([['dish_type','=','desserts'],['is_choice','=', 1]])->get();
        // return($appetizer);
        return view('welcome')->with('appetizers', $appetizers)->with('mains', $mains)->with('desserts',$desserts);
    }

    public function contact_us(){
        return view('contact');
    }
    public function homeCook(){
        return view('home_cook');
    }
    public function menu(){

       $appetizers = MenuModel::select('*')->where('dish_type','=','appetizer')->get();
        $mains = MenuModel::select('*')->where('dish_type','=','main dish')->get();
        $desserts = MenuModel::select('*')->where('dish_type','=','desserts')->get();
        // return($appetizer);
        return view('menu')->with('appetizers', $appetizers)->with('mains', $mains)->with('desserts',$desserts);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
