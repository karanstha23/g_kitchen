<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    use HasFactory;

    protected $table= 'menu_models';
    protected $fillable = ['dish_type', 'dish_name', 'dish_description', 'dish_price', 'is_choice']
;}
