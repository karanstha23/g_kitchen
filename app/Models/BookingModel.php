<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingModel extends Model
{
    use HasFactory;

    protected $table = 'booking';

    protected $fillable = ['name','email', 'contact_number', 'guest', 'date', 'time', 'request', 'user_id'];
}
