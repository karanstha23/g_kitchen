@include('layouts/header')
    {{-- <div class="container" style="width: 100%"> --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="#"><img src="images/icon.png" alt="" style="height: 60px; width:200px"></a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/"><i class="fas fa-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/menu"><i class="fas fa-bars"></i> Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/contact"><i class="fas fa-id-card"></i> Contact Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/home-cook"><i class="fas fa-blender"></i> Home Cook</a>
                  </li>

                  {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Dropdown link
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <li><a class="dropdown-item" href="#">Action</a></li>
                      <li><a class="dropdown-item" href="#">Another action</a></li>
                      <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                  </li> --}}
                </ul>
              </div>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->is_admin==0)
                                <a class="dropdown-item" href="/home"
                                       >
                                        {{ __('MY Bookings') }}
                                    </a>
                                    @endif
                                    @if(Auth::user()->is_admin==1)
                                <a class="dropdown-item" href="/admin/home"
                                       >
                                        {{ __('Admin Panel') }}
                                    </a>
                                    @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>


            </div>
        </nav>
        <div class="container col-md-12">
            <img src="images/rest.jpg" class="img-fluid" alt="Image Background" style="width: 100%; height: 90vh; object-fit: cover">
        </div>


        <div class="front_button">
            <button class="btn booking_button"id="book_btn" type="submit">Book a Table</button>
        </div>

        <div class="container pt-4 pb-4 px-3">
            <div class="row">
                <div class="col-md-9">
<h3 class="font-weight-bold">Chefs Choice</h3>
<h4 class="font-weight-bold border-bottom pt-2 pb-2 px-3">Appetizer</h4>

@foreach($appetizers as $appetizer)
<h5 class="mt-3 px-3">{{$appetizer->dish_name}} <span style="letter-spacing: 4px;">...........................................................................</span> ${{$appetizer->dish_price}}</h5>
<p class="px-3">{{$appetizer->dish_description}}</p>
@endforeach


<h4 class="font-weight-bold border-bottom pt-2 pb-2 px-3">Main Dish</h4>

@foreach($mains as $main)
<h5 class="mt-3 px-3">{{$main->dish_name}} <span style="letter-spacing: 4px;">...........................................................................</span> ${{$main->dish_price}}</h5>
<p class="px-3">{{$main->dish_description}}</p>
@endforeach
<h4 class="font-weight-bold border-bottom pt-2 pb-2 px-3">Desserts</h4>

@foreach($desserts as $dessert)
<h5 class="mt-3 px-3">{{$dessert->dish_name}} <span style="letter-spacing: 4px;">...........................................................................</span> ${{$dessert->dish_price}}</h5>
<p class="px-3">{{$dessert->dish_description}}</p>
@endforeach

                </div>
                <div class="col-md-3">
                    <div class="container pt-4 pb-4 text-center">
                        <h4>News Letter</h4>
                        <h5>Subscribe To Receive Our Latest News</h5>
<p>Subscribe to our newsletter
    to receive our latest news & promotions!</p>
    <form class="d-flex">
        <input class="form-control me-2 mr-2" type="search" placeholder="Email Address" aria-label="Search">
        <button class="btn btn-outline-success booking_button_down" type="submit">Submit</button>
      </form>

      <h4 class="pt-4">Blog</h4>
                        <h5>Best Resturant in Town</h5>
<p>Greasy Kitchen is one of the best food provider in the town. All healthy food with home as well as site delivery facility.</p>
<p><a href="#">Read More</a></p>

<h5>Love the Food</h5>
<p>Greasy Kitchen is one of the best food provider in the town. All healthy food with home as well as site delivery facility.</p>
<p><a href="#">Read More</a></p>
                    </div>

                </div>
            </div>
        </div>

        <div class="container col-md-12" id="booking_form">

            <div class="row">
                <div class="col-md-3  px-5 pt-5 pb-5 text-white">
                    <h2>Booking</h2>
                    <p>You can book a table by completing this form.</p>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>

                @endif
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                </div>
                <form class="row g-3 col-md-6  px-5 pt-5 pb-5 text-white" method="POST" action="/admin/home/booking">
                    @csrf
                    <div class="col-md-12">

                    </div>
                    @auth
                    <input type="hidden" name="user_id" class="form-control" value="{{Auth::user()->id}}" placeholder="Enter your Name" id="inputName4" required>
                    @endauth
                    <div class="col-md-6">
                      <label for="inputEmail4" class="form-label">Name</label>
                      <input type="name" name="name" value="{{old('name')}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                    </div>
                    <div class="col-md-6">
                      <label for="inputEmail" class="form-label">Email</label>
                      <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Enter your Email" id="inputEmail4" required>
                    </div>
                    <div class="col-6">
                      <label for="inputAddress" class="form-label">Contact Number</label>
                      <input type="text" class="form-control" value="{{old('contact_number')}}" name="contact_number" id="inputAddress" placeholder="Contact Nmuber" required>
                    </div>
                    <div class="col-6">
                      <label for="inputAddress2" class="form-label">No. of Guests</label>
                      <input type="text" class="form-control" name="guest" value="{{old('guest')}}" id="inputAddress2" placeholder="Enter number of Guests" required>
                    </div>
                    <div class="col-md-6">
                      <label for="inputCity" class="form-label">Date</label>
                      <input type="date" class="form-control" value="{{old('date')}}" name="date" id="inputCity" required>
                    </div>
                    <div class="col-md-6">
                        <label for="inputCity" class="form-label">Time</label>
                        <input type="time" name="time" class="form-control" value="{{old('time')}}" id="inputCity" required>
                      </div>
                      <div class="col-md-12">
                        <label for="inputCity" class="form-label">Special Request</label>
                        <textarea name="request" class="form-control" id="request" cols="30" rows="5">{{old('request')}}</textarea>
                        {{-- <input type="time" name="time" class="form-control" name="date" id="inputCity" required> --}}
                      </div>

                    <div class="col-12 pt-4">
                      <button type="submit" class="btn btn-primary booking_button_down col-12">Book a Table</button>
                    </div>
                  </form>
            </div>



        </div>

        <div class="container pt-4">
            <div class="row">
                <div class="col-md-4">
<div class="row">
    <div class="col-md-3">
        <i class="fas fa-location-arrow fa-2x px-4"></i>
    </div>
    <div class="col-md-9">
        93 Auburn Road, Auburn, NSW 2144, Sydney
    </div>
</div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-3">
                                <i class="fas fa-phone fa-2x px-4"></i>
                            </div>
                            <div class="col-md-9">
                                Booking: (012) 345 - 6789 <br>
Inquiries: 555 - 674 0097
                            </div>
                        </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <i class="fas fa-envelope fa-2x px-4"></i>
                                                    </div>
                                                    <div class="col-md-9">
                                                        info@contact-restaurant.com
sales@contact-restaurant.com
                                                    </div>
                                                </div>
                                                                    </div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
            </div>
        </div>

        <div class="container text-center pt-5 pb-3">
            <p class=" border-top border-bottom pt-1 pb-1">Copyright © 2021 Greasy Kitchen. all rights reserved.</p>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script>
            $("#book_btn").click(function() {
                $('html, body').animate({
                    scrollTop: $("#booking_form").offset().top
                }, 2000);
            });
        </script>
</body>
</html>
