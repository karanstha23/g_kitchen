@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr class="text-white table">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>No of Guests</th>
                        <th>Date and Time</th>
                        <th>Request</th>
                        <th style="text-align: right;">Actions</th>
                    </tr>
                </thead>
                <tbody>

                        @foreach($bookings as $booking)
                        <tr>
                        <td>{{$booking->id}}</td>
                        <td>{{$booking->name}}</td>
                        <td>{{$booking->email}}</td>
                        <td>{{$booking->contact_number}}</td>
                        <td>{{$booking->guest}}</td>
                        <td>{{$booking->date}} {{$booking->time}}</td>
                        <td>{{$booking->request}}</td>
                        @if($booking->is_accepted == 0)
                           <td> <a href="/home/{{$booking->id}}"><button class="btn btn-success">Confirm</button></a></td>

                        @elseif($booking->is_accepted == 1)
                            <td>Confirmed</td>

                        @endif
                    </tr>
                        @endforeach



                </tbody>

            </table>

        </div>
    </div>
</div>
@endsection

