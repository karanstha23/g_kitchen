@include('layouts/header')
    {{-- <div class="container" style="width: 100%"> --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="/"><img src="/images/icon.png" alt="" style="height: 60px; width:200px"></a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/"><i class="fas fa-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/menu"><i class="fas fa-bars"></i> Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="/greasy_spoon/contact"><i class="fas fa-id-card"></i> Contact Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/home-cook"><i class="fas fa-blender"></i> Home Cook</a>
                  </li>
                  {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Dropdown link
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <li><a class="dropdown-item" href="#">Action</a></li>
                      <li><a class="dropdown-item" href="#">Another action</a></li>
                      <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                  </li> --}}
                </ul>
              </div>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->is_admin==0)
                                <a class="dropdown-item" href="/home"
                                       >
                                        {{ __('MY Bookings') }}
                                    </a>
                                    @endif
                                    @if(Auth::user()->is_admin==1)
                                <a class="dropdown-item" href="/admin/home"
                                       >
                                        {{ __('Admin Panel') }}
                                    </a>
                                    @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>


            </div>
        </nav>

        <div class="container pt-4">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10">
                        <h4>Contact Us</h4>
                        <form class="" method="POST" action="{{ route('contact.store') }}">
                            @csrf
                            <div class="col-md-12">
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>

                            @endif
                            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                            </div>

                            <div class="col-md-6">
                              <label for="inputEmail4" class="form-label">Name</label>
                              <input type="name" name="name" value="{{old('name')}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                            </div>
                            <div class="col-md-6">
                              <label for="inputEmail" class="form-label">Email</label>
                              <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Enter your Email" id="inputEmail4" required>
                            </div>
                            <div class="col-6">
                              <label for="inputAddress" class="form-label">Contact Number</label>
                              <input type="text" class="form-control" value="{{old('contact_number')}}" name="contact_number" id="inputAddress" placeholder="Contact Nmuber" required>
                            </div>

                              <div class="col-md-12">
                                <label for="inputCity" class="form-label">Message</label>
                                <textarea name="message" class="form-control" id="message" cols="30" rows="5">Enter your message</textarea>
                                {{-- <input type="time" name="time" class="form-control" name="date" id="inputCity" required> --}}
                              </div>

                            <div class="col-12 pt-4">
                              <button type="submit" class="btn btn-primary booking_button_down col-12">Submit Message</button>
                            </div>
                          </form>
                    </div>
                    <div class="col-md-2">

                    </div>
                </div>
            </div>
        </div>

        <div class="container text-center pt-5 pb-3">
            <p class=" border-top border-bottom pt-1 pb-1">Copyright © 2021 Greasy Kitchen. all rights reserved.</p>
        </div>


</body>
</html>
