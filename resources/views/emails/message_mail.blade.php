<!DOCTYPE html>
<html>
<head>
    <title>Greasy Spoon</title>
</head>
<body>
    <h1>User Message</h1>
    <p>{{$details['name']}} has sent you a message</p>
    <p>Message: {{$details['message']}}</p>
    <p>Details:</p>
    <p>Contact Number: {{$details['contact_number']}}</p>
    <p>Email: {{$details['email']}}</p>

    <p>Thank you</p>
</body>
</html>
