@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Edit Menu
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('menu.update', $menus->id)}}">
                        @csrf
                        @method('PUT')
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Dish Type</label>
                            <input type="name" name="dish_type" value="{{$menus->dish_type}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                        </div>
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Dish Name</label>
                            <input type="name" name="dish_name" value="{{$menus->dish_name}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                        </div>
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Dish Description</label>
                            <input type="name" name="dish_description" value="{{$menus->dish_description}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                        </div>
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Dish Price</label>
                            <input type="name" name="dish_price" value="{{$menus->dish_price}}" class="form-control" placeholder="Enter your Name" id="inputName4" required>
                        </div>

                        <div class="col-md-12 text-center mt-3">
                            <button type="submit" class="btn btn-primary col-md-6">Submit</button>
                        </div>
                      </form>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

