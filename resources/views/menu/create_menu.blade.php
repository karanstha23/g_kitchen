@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11">
<div class="row">

    <button type="btn" onclick="location.href='/admin/home/menu'" class="btn btn-primary col-md-3 mb-3">View Menu</button>
</div>


            <div class="card">

            <div class="card-header">
                Create Menu
            </div>
            <div class="card-body">
                <form method="POST" action="/admin/home/menu">
                    @csrf
                    <div class="col-md-12">
                        <label for="inputEmail4" class="form-label">Dish Type</label>
                        <input type="name" name="dish_type" value="{{old('dish_type')}}" class="form-control" placeholder="Enter Dish Type" id="inputName4" required>
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail4" class="form-label">Dish Name</label>
                        <input type="name" name="dish_name" value="{{old('dish_type')}}" class="form-control" placeholder="Enter Dish Name" id="inputName4" required>
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail4" class="form-label">Dish Description</label>
                        <input type="name" name="dish_description" value="{{old('dish_type')}}" class="form-control" placeholder="Enter Dish Description" id="inputName4" required>
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail4" class="form-label">Dish Price</label>
                        <input type="name" name="dish_price" value="{{old('dish_type')}}" class="form-control" placeholder="Enter Dish Price" id="inputName4" required>
                    </div>
                    <div class="col-md-12 text-center mt-3">
                        <button type="submit" class="btn btn-primary col-md-6">Submit</button>
                    </div>

                  </form>
            </div>
            </div>


        </div>
    </div>
</div>
@endsection

