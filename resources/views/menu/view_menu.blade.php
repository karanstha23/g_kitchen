@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <button type="btn" onclick="location.href='/admin/home/menu/create'" class="btn btn-primary col-md-3 mb-3">Create Menu</button>
    </div>
    <div class="row justify-content-center">

        <div class="col-md-11">
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr class=" table text-white">
                        <th>ID</th>
                        <th>Dish Type</th>
                        <th>Dish Name</th>
                        <th>Dish Description</th>
                        <th>Dish Price</th>
                        <th>Chef's Choice</th>

                        <th style="text-align: right;">Actions</th>
                    </tr>
                </thead>
                <tbody>

                        @foreach($menus as $menu)
                        <tr>
                        <td>{{$menu->id}}</td>
                        <td>{{$menu->dish_type}}</td>
                        <td>{{$menu->dish_name}}</td>
                        <td>{{$menu->dish_description}}</td>
                        <td>{{$menu->dish_price}}</td>
                        @if($menu->is_choice == 0)
                            <td><a href="/admin/home/menu/activate/{{$menu->id}}"><button class="btn btn-success">Activate</button></a></td>

                        @elseif($menu->is_choice == 1)
                            <td><a href="/admin/home/menu/deactivate/{{$menu->id}}"><button class="btn btn-success">Deactivate</button></a></td>

                        @endif



                           <td> <a href="/admin/home/menu/{{$menu->id}}/edit"><button class="btn btn-success">Edit</button></a></td>


                    </tr>
                        @endforeach



                </tbody>

            </table>

        </div>
    </div>
</div>
@endsection

