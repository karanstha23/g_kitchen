@extends('layouts.u_app')

@section('content')
<div class="container">
    <div class="row">

        <button type="btn" onclick="location.href='/'" class="btn btn-primary col-md-3 mb-3">Book a Table</button>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    Bookings List
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="mydata">
                        @if($bookings->count()>0)
                        <thead>
                            <tr class="table text-white">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact Number</th>
                                <th>No of Guests</th>
                                <th>Date and Time</th>
                                <th>Request</th>
                                <th style="text-align: right;">Decision</th>
                            </tr>
                        </thead>
                        <tbody>

                                @foreach($bookings as $booking)
                                <tr>
                                <td>{{$booking->id}}</td>
                                <td>{{$booking->name}}</td>
                                <td>{{$booking->email}}</td>
                                <td>{{$booking->contact_number}}</td>
                                <td>{{$booking->guest}}</td>
                                <td>{{$booking->date}} {{$booking->time}}</td>
                                <td>{{$booking->request}}</td>
                                @if($booking->is_accepted == 0)
                                    <td>Pending</td>

                                @elseif($booking->is_accepted == 1)
                                    <td>Confirmed</td>

                                @endif
                            </tr>
                                @endforeach


                        </tbody>
                        @else
                        <h3>No Bookings Record</h3>

                        @endif
                    </table>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

