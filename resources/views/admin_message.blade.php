@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-11">
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr class=" table text-white">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Message</th>

                    </tr>
                </thead>
                <tbody>

                        @foreach($messages as $message)
                        <tr>
                        <td>{{$message->id}}</td>
                        <td>{{$message->name}}</td>
                        <td>{{$message->email}}</td>
                        <td>{{$message->contact_number}}</td>
                        <td>{{$message->message}}</td>



                    </tr>
                        @endforeach



                </tbody>

            </table>

        </div>
    </div>
</div>
@endsection

