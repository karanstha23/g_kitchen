@include('layouts/header')
    {{-- <div class="container" style="width: 100%"> --}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="#"><img src="/images/icon.png" alt="" style="height: 60px; width:200px"></a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/"><i class="fas fa-home"></i> Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/menu"><i class="fas fa-bars"></i> Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/greasy_spoon/contact"><i class="fas fa-id-card"></i> Contact Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="/greasy_spoon/home-cook"><i class="fas fa-id-card"></i> Home Cook</a>
                  </li>
                  {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Dropdown link
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <li><a class="dropdown-item" href="#">Action</a></li>
                      <li><a class="dropdown-item" href="#">Another action</a></li>
                      <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                  </li> --}}
                </ul>
              </div>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-sign-in-alt"></i> {{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->is_admin==0)
                                <a class="dropdown-item" href="/home"
                                       >
                                        {{ __('MY Bookings') }}
                                    </a>
                                    @endif
                                    @if(Auth::user()->is_admin==1)
                                <a class="dropdown-item" href="/admin/home"
                                       >
                                        {{ __('Admin Panel') }}
                                    </a>
                                    @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>


            </div>
        </nav>
        <div class="jumbotron">
            <h4>Future Works</h4>
            <p>This page home cooks will contain the number of grocery items which are used to cook delicious food in our kitchen.
                People can dirsectly buy the goods from here which will be either home delivered or pick up from the store.
            </p>
            <p>
                Furthermore, with the integration of online payment gateway, people can pay online for their shopping.
            </p>

        </div>

        <div class="container text-center pt-5 pb-3">
            <p class=" border-top border-bottom pt-1 pb-1">Copyright © 2021 Greasy Kitchen. all rights reserved.</p>
        </div>


</body>
</html>
